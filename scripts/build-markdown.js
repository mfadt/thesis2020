#!/usr/local/bin/node

/* -------------------------------
   Utility script for building
   markdown files from spreadsheet
   ------------------------------- */

const path = require("path");
const fs = require("fs");
const glob = require("glob");
const xlsx = require("node-xlsx");
const chalk = require("chalk");

const data_path = path.resolve(__dirname, "..", "data.xlsx");
const data = xlsx.parse(data_path);

const People_Data = data.filter(i => i.name === "People")[0];
const Project_Data = data.filter(i => i.name === "Projects")[0];

/* --------------
   Generate Files
   -------------- */

delete_files();
writeData(People_Data.data, "people");
writeData(Project_Data.data, "projects");

/* -----------------
   Hoisted Functions
   ----------------- */

function format_slug(text) {
  return text
    .toLowerCase()
    .replace(/[^a-zA-Z0-9]/g, "-")
    .replace(/ /g, "-");
}

function xlsx_to_json(data) {
  let headers = data.splice(0, 1).flat();
  let content = data;

  let json = content.map((item, position) => {
    let entry = {};
    headers.forEach((header, position) => {
      entry[header] = item[position];
    });
    let slug = entry["title"] || entry["name"] || "";
    entry["slug"] = format_slug(slug);
    return entry;
  });

  return json;
}

function people_json_to_md(entry) {
  return `---
name: ${entry.name}
slug: ${entry.slug}
website: ${entry.website || "https://mfadt.parsons.edu"}
---

${entry.biography || "Empty"}
  `;
}

function project_json_to_md(entry) {
  return `---
title: ${entry.title}
slug: ${entry.slug}
website: ${entry.website || ""}
---

${entry.description || ""}
  `;
}

function delete_files() {
  // delete existing markdown files
  glob.sync(`${__dirname}/../content/**/*.md`).forEach(file => {
    fs.unlinkSync(file);
  });
}

// for each entry in Project_Data,
// write file with markdown formatting
function writeData(dataset, folder) {
  let written_files = [];
  xlsx_to_json(dataset).forEach(entry => {
    let data;

    if (folder === "projects") {
      data = project_json_to_md(entry);
    } else {
      data = people_json_to_md(entry);
    }

    let filepath = path.resolve(
      __dirname,
      "..",
      "content",
      folder,
      entry.slug + ".md"
    );

    fs.writeFileSync(filepath, data, { encoding: "utf8" });
    written_files.push(entry.slug + ".md");
  });

  console.log(chalk.green("/" + folder + ""));
  written_files.forEach(line => {
    console.log(`• ${line}`);
  });
  console.log("");
}
