// CHANGES TO THIS FILE REQUIRE A SERVER RESTART

const config = {
  title: "Hindsight",
  subtitle: "MFA Design + Technology Thesis Show 2020",
  description: "",
  url: "https://mfadt.parsons.edu"
};

// CONFIG OBJECT
module.exports = {
  siteName: config.title,
  siteUrl: config.title,
  host: process.env.HOSTNAME,
  port: 8080,
  metadata: {
    siteName: config.title,
    siteDescription: config.description,
    title: config.title,
    subtitle: config.subtitle
  },
  outputDir: "dist",
  plugins: [
    {
      use: "gridsome-plugin-tailwindcss"
    },
    {
      // CREATES PAGES FOR MARKDOWN FILES
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "Projects",
        baseDir: "./content/projects/",
        path: "*.md",
        resolveAbsolutePaths: true,
        remark: {
          externalLinksTarget: "_blank",
          externalLinksRel: ["nofollow", "noopener", "noreferrer"]
        }
      }
    },
    {
      // CREATES PAGES FOR MARKDOWN FILES
      use: "@gridsome/source-filesystem",
      options: {
        typeName: "People",
        baseDir: "./content/people/",
        path: "*.md",
        resolveAbsolutePaths: true,
        remark: {
          externalLinksTarget: "_blank",
          externalLinksRel: ["nofollow", "noopener", "noreferrer"]
        }
      }
    },
    {
      // PLUGIN FOR GENERATING A SITEMAP
      use: "@gridsome/plugin-sitemap",
      options: {
        cacheTime: 600000, // default
        exclude: ["/exclude-me"],
        config: {
          "/": {
            changefreq: "weekly",
            priority: 0.5
          },
          "/*": {
            changefreq: "weekly",
            priority: 0.5
          }
        }
      }
    }
  ],
  templates: {
    People: [
      {
        path: node => {
          return `/people/${node.slug}`;
        }
      }
    ],
    Projects: [
      {
        path: node => {
          return `/project/${node.slug}`;
        }
      }
    ]
  }
};
