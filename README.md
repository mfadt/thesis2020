# THESIS 2020

## SETTING UP LOCAL DEV ENVIRONMENT

### REQUIREMENTS

- Node.js
- npm

### STEPS

clone project

```sh
git clone https://gitlab.com/mfadt/thesis2020.git
```

cd into directory

```sh
cd thesis2020
```

install modules

```sh
npm install
```

run local server

```sh
npm run dev
```

server will be running at http://localhost:8080
